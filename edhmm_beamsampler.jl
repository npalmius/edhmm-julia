# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 25-Aug-2014

# Reference:
# Dewar, M., Wiggins, C., & Wood, F. (2012). Inference in Hidden Markov
# Models with Explicit State Duration Distributions. Signal Processing
# Letters, IEEE, 19(4), 235–238.
# Retrieved from http://dx.doi.org/10.1109/LSP.2012.2184795

using EDHMM
using Distributions
using MLBase
using Plotly

include("..\\plotly-julia\\plotly_signin.jl");

# Computes the probability of an initial duration x from distribution N.
function pdf_duration(N, x)
    if ceil(x) <= 1
        return cdf(N, max(ceil(x), 1));
    else
        return cdf(N, ceil(x)) - cdf(N, ceil(x) - 1);
    end
end

# Returns the mode of the provided distribution, dealing with invalid distributions.
function get_mode(duration_dist)
    if (typeof(duration_dist) <: Gamma) && (duration_dist.shape < 1)
        return 0;
    else
        return convert(Int64, max(floor(Distributions.mode(duration_dist)), 1));
    end
end

# Returns the worst case duration in duration_dist conditioned on slice u.
function get_duration_upper_bound(u, duration_dist)
    # Start at the mode of the distribution.
    d_mode = get_mode(duration_dist);
    # Since we floored the mode, we must start at d_mode+1.
    d_diff = 1;

    while pdf_duration(duration_dist, d_mode + d_diff) > u
        d_diff += 1;
    end

    return d_mode + d_diff;
end

# Adds initial duration probabilities to α matrix conditioned on slice u.
function add_new_duration_propabilities(α, u, duration_dist, p_transition, p_observation, α_prev)
    # Start at the mode of the distribution.
    d_mode = get_mode(duration_dist);
    # Since we floored the mode, we must compare both d_mode and d_mode+1 so start at d_mode+1.
    d_diff = 1;

    # From the mode, work left and right while probability is > u.
    while true
        duration = d_mode + d_diff;
        p_d_s = pdf_duration(duration_dist, duration) * p_transition;
        if (p_d_s <= u) && (d_diff > 0)
            # We have added all durations > the mode which are > u, so start working down.
            d_diff = 0;
        elseif (p_d_s <= u) && (d_diff <= 0)
            # We have added all durations < the mode which are > u so break from loop.
            break;
        elseif (p_d_s > u)
            # duration d + d_diff is < u so add to α matrix.
            duration_i = convert(Int64, duration);

            α[duration_i] += (p_observation * α_prev)

            # Increment or decrement offset from mode.
            if d_diff == 0
                d_diff = -1;
            else
                d_diff += sign(d_diff);
            end
        else
            break; # This should never happen
        end
        if (d_mode + d_diff) < 1
            break;
        end
    end
end

function edhmm_beam_sample(data)
    # Algorithm 2  Sample the EDHMM
    # Initialise parameters A, λ, θ: Initialize u_t small ∀ T
    # for sweep ∈ {1, 2, 3, ...} do
    #   Forward: run (7) to get α_t(zt) given U and Y ∀ T
    #   Backward: sample z_T ~ α_T(z_T)
    #   for t ∈ {T, T - 1, ..., 1} do
    #     sample z_t-1 ~ I(u_t < p(z_t|z_t-1))α_t-1(z_t-1)
    #   end for
    #   Slice:
    #   for t ∈ {1, 2, ..., T} do
    #     evaluate l = p(d_t|x_τ,d_t-1)p(x_t>x_t-1,d_t-1)
    #     sample u_t ~ Uniform(0, l)
    #   end for
    #   sample parameters A, λ, θ
    # end for

    T = length(data.y);

    S = length(data.θ.o);
    S_VALID = find(data.θ.o);

    O_Δ = 0.001;
    
    multivariate = false;

    mv_dim = 0;
    mv_dist = nothing;

    if typeof(data.θ.o[1]) <: UnivariateDistribution
    elseif typeof(data.θ.o[1]) <: MultivariateDistribution
        T = size(data.y, 2);
        multivariate = true;

        first_dim = 0
        all_diag = true;

        for s = S_VALID
            if first_dim == 0
                first_dim = data.θ.o[s].dim;
                @assert (size(data.y, 1) == first_dim) "Multivariate observation distribution dimensionality does not correspond with data dimensionality."
            else
                @assert (data.θ.o[s].dim == first_dim) "All multivariate observation distribution must have the same number of dimensions."
            end
            full_Σ = full(data.θ.o[s].Σ);
            all_diag = all_diag && (full_Σ .* eye(first_dim) == full_Σ); # "Only diagonal multivariate normal distributions are currently supported."
        end
        if all_diag
            mv_dim = first_dim;
            mv_dist = cell(S, first_dim);
            for s = S_VALID
                full_Σ = full(data.θ.o[s].Σ);
                for d = 1:first_dim
                    mv_μ = data.θ.o[s].μ[d];
                    mv_Σ = full_Σ[d, d];
                    mv_dist[s, d] = Normal(mv_μ, sqrt(mv_Σ));
                end
            end
        end
    else
        @assert false "Invalid observation distribution"
    end

    SWEEPS = 100;
    SAMPLES = 100;

    sampled_log_lik = ones(SWEEPS, 1) * NaN;
    sampled_z_best = 0;
    sampled_d_best = 0;
    α_best = 0;
    α_stored = cell(SWEEPS, 1);
    u_stored = ones(SWEEPS, T) * NaN;
    z_stored = ones(SWEEPS, T) * NaN;
    l_stored = ones(SWEEPS, T) * NaN;

    # Initialise parameters A, λ, θ: Initialize u_t small ∀ T
    u = ones(T, 1) / 100;
    l = ones(T, 1) * NaN;

    # for sweep ∈ {1, 2, 3, ...} do
    #sweep = 1
    for sweep = 1:SWEEPS
        print("Sweep ");
        print(sweep);
        print(" : ");

        u_stored[sweep, :] = u;
        l_stored[sweep, :] = l;

        # Initialise α as a cell matrix of time and states.
        # Each state will contain a variable length array with state durations.
        α = cell(T, S);
        α_values_T = 0;

        # Forward: run (7) to get α_t(zt) given U and Y ∀ T
        for t = 1:T
            #print(" Forward t = ");
            #println(t);

            # Normalising factor
            α_total = 0.0;
            for s = S_VALID
                #print("  state ");
                #println(s);

                # Compute upper bound for durations
                upper_bound = get_duration_upper_bound(u[t], data.θ.d[s]);
                if t > 1
                    upper_bound = max(upper_bound, length(α[t - 1, s]));
                end

                # Create duration vector
                α[t, s] = zeros(upper_bound, 1);

                p_observation = 1;

                # Probability of observation given state s
                if multivariate
                    if mv_dim > 0
                        for d = 1:mv_dim
                            if O_Δ > 0
                                p_observation *= ((Distributions.cdf(mv_dist[s, d], data.y[d, t] + O_Δ) - Distributions.cdf(mv_dist[s, d], data.y[d, t] - O_Δ)) / (2 * O_Δ));
                            else
                                p_observation *= Distributions.pdf(mv_dist[s, d], data.y[d, t]);
                            end
                        end
                    else
                        p_observation = Distributions.pdf(data.θ.o[s], data.y[:, t]);
                    end
                else
                    if O_Δ > 0
                        p_observation = ((Distributions.cdf(data.θ.o[s], data.y[t] + O_Δ) - Distributions.cdf(data.θ.o[s], data.y[t] - O_Δ)) / (2 * O_Δ));
                    else
                        p_observation = Distributions.pdf(data.θ.o[s], data.y[t]);
                    end
                end

                if t == 1
                    # Transition probability at time t = 1 is from initial state distribution
                    p_transition = Distributions.pdf(data.θ.π, s);
                    add_new_duration_propabilities(α[t, s], u[t], data.θ.d[s], p_transition, 1, 1);
                else
                    # For all durations in time t-1 > 1, add probability that we will stay in the same state
                    for duration_prev = 1:length(α[t - 1, s])
                        if duration_prev > 1
                            duration_prev_α = α[t - 1, s][duration_prev]
                            α[t, s][duration_prev - 1] += (1 * duration_prev_α)
                        end
                    end

                    # Calculate probability of transitioning from all other states with duration = 1
                    for s_prev = S_VALID
                        p_transition = Distributions.pdf(data.θ.a[s_prev], s);
                        α_prev = α[t - 1, s_prev][1];
                        if (p_transition > 0) && (α_prev > 0)
                            add_new_duration_propabilities(α[t, s], u[t], data.θ.d[s], p_transition, 1, α_prev);
                        end
                    end
                end

                for duration = find(α[t, s])
                    α[t, s][duration] *= p_observation;
                end

                # Add total α values for durations in state to normalising factor
                α_total += sum(α[t, s]);
            end

            # Normalise α values to sum to 1.
            for s = S_VALID
                if α_total > 0
                    for k = 1:length(α[t, s])
                        v = α[t, s][k];
                        α[t, s][k] /= α_total;
                        if α[t, s][k] < 0
                            println("Warning: normalised alpha value < 0: ");
                            print(α[t, s][k])
                            print(" Original value: ");
                            print(v);
                            print(" Normalising factor: ");
                            println(α_total);
                        end
                    end
                end
                if t == T
                    α_values_T += length(α[t, s]);
                end
            end
        end

        α_stored[sweep] = α;

        #println(" Forward complete");

        sample_valid = ones(SAMPLES, 1);
        sampled_z = zeros(T, SAMPLES);
        sampled_d = zeros(T, SAMPLES);

        last_t = 0;

        # Backward:
        # for t ∈ {T, T - 1, ..., 1} do
        for t = T:-1:1
            last_t = t;
            for r = find(sample_valid)
                if t == T
                    # sample z_T ~ α_T(z_T)

                    # At time t = T, all states in α are valid.
                    α_z = zeros(α_values_T, 1);
                    α_d = zeros(α_values_T, 1);
                    α_p = zeros(α_values_T, 1);

                    i = 1;

                    for s = S_VALID
                        for k = 1:length(α[t, s])
                            α_z[i] = s;
                            α_d[i] = k;
                            α_p[i] = α[t, s][k];
                            i += 1;
                        end
                    end
                else
                    # sample z_t-1 ~ I(u_t < p(z_t|z_t-1))α_t-1(z_t-1)

                    # At time t < T, the previous state can be any state at duration 1, or
                    # the same state with duration d + 1.
                    α_z = zeros(S+1, 1);
                    α_d = zeros(S+1, 1);
                    α_p = zeros(S+1, 1);

                    # First add probability of staying in the same state with duration d + 1.
                    # This will always be above the slice.
                    α_z[1] = sampled_z[t+1, r];
                    α_d[1] = sampled_d[t+1, r] + 1;
                    α_p[1] = get(α[t, sampled_z[t+1, r]], convert(Int64, sampled_d[t+1, r] + 1), 0.0);

                    i = 2;

                    # Calculate probability of starting at this duration ...
                    p_duration = pdf_duration(data.θ.d[sampled_z[t+1, r]], sampled_d[t+1, r]);

                    # ... if this is below the slice, don't bother checking individual states/
                    if p_duration > u[t+1]
                        for s = S_VALID
                            # For each state, check the probability of transition.
                            p_transition = Distributions.pdf(data.θ.a[s], sampled_z[t+1, r]);

                            # Check if this is above the slice.
                            if (p_transition * p_duration) > u[t+1]
                                α_z[i] = s;
                                α_d[i] = 1;
                                α_p[i] = α[t, s][1];
                                i += 1;
                            end
                        end
                    end
                end

                # Check if there are any valid transitions. If not, abort this sample run.
                if sum(α_p) == 0
                    sample_valid[r] = 0;
                    continue;
                end

                # Normalise probability vector.
                α_p_orig = α_p;
                α_p = α_p / sum(α_p);


                # Draw state from possible values.
                sampled_i = try
                    rand(Distributions.Categorical(vec(α_p)));
                catch
                    -1;
                end

                if sampled_i == -1
                    println("Error creating Categorical with vector:");
                    for i = 1:length(α_p)
                        print("    ");
                        println(α_p[i]);
                    end
                    println("Original (un-normalised) vector:");
                    for i = 1:length(α_p_orig)
                        print("    ");
                        println(α_p_orig[i]);
                    end
                    sample_valid[r] = 0;
                    continue;
                end

                # Save sampled state and duration
                sampled_z[t, r] = α_z[sampled_i];
                sampled_d[t, r] = α_d[sampled_i];
            end

            # Check if all sample runs have reached a dead-end.
            if sum(sample_valid) == 0
                break;
            end
        end # end for (t ∈ {T, T - 1, ..., 1} do)

        #println(" Backward complete");

        # Check if all sample runs reached a dead-end.
        if sum(sample_valid) == 0
            print("No valid samples at run ");
            print(last_t);
            println(". Aborting sweep.");

            # re-sample u_t ~ Uniform(0, l) from last sweep
            for t = 1:T
                if isnan(l[t])
                    if u[t] == 1/100
                        u[t] /= 1000;
                    else
                        @assert false "Cannot reduce u further."
                    end
                else
                    u[t] = rand(Distributions.Uniform(0, l[t]));
                end
            end

            continue;
        else
            valid_samples = sum(sample_valid) / SAMPLES;
        end

        sweep_max_log_lik = -Inf;
        sweep_max_sample_i = 0;

        # Slice:
        # for t ∈ {1, 2, ..., T} do
        # For all valid sample runs, compute log likelihood and l.
        for r = find(sample_valid)
            sample_log_lik = 0;
            sample_l = zeros(T, 1);

            for t = 1:T
                # evaluate l = p(d_t|x_τ,d_t-1)p(x_t>x_t-1,d_t-1)
                sample_l[t] = 1;
                if t == 1
                    sample_l[t] *= Distributions.pdf(data.θ.π, sampled_z[t, r]);
                    sample_l[t] *= pdf_duration(data.θ.d[sampled_z[t, r]], sampled_d[t, r]);
                    sample_log_lik += log(sample_l[t]);
                elseif (sampled_z[t, r] != sampled_z[t-1, r]) || (sampled_d[t-1, r] == 1)
                    sample_l[t] *= Distributions.pdf(data.θ.a[sampled_z[t-1, r]], sampled_z[t, r]);
                    sample_l[t] *= pdf_duration(data.θ.d[sampled_z[t, r]], sampled_d[t, r]);
                    sample_log_lik += log(sample_l[t]);
                elseif (sampled_z[t, r] == sampled_z[t-1, r]) || (sampled_d[t, r] == sampled_d[t-1, r] - 1)
                    # Do nothing
                    sample_l[t] *= 1;
                else
                    @assert false "calculation of l"
                    # This should never happen!
                end

                if multivariate
                    if mv_dim > 0
                        for d = 1:mv_dim
                            if O_Δ > 0
                                sample_log_lik += log((Distributions.cdf(mv_dist[sampled_z[t, r], d], data.y[d, t] + O_Δ) - Distributions.cdf(mv_dist[sampled_z[t, r], d], data.y[d, t] - O_Δ)) / (2 * O_Δ));
                            else
                                sample_log_lik += Distributions.logpdf(mv_dist[sampled_z[t, r], d], data.y[d, t]);
                            end
                        end
                    else
                        sample_log_lik += Distributions.logpdf(data.θ.o[sampled_z[t, r]], data.y[:, t]);
                    end
                else
                    if O_Δ > 0
                        sample_log_lik += log((Distributions.cdf(data.θ.o[sampled_z[t, r]], data.y[t] + O_Δ) - Distributions.cdf(data.θ.o[sampled_z[t, r]], data.y[t] - O_Δ)) / (2 * O_Δ));;
                    else
                        sample_log_lik += Distributions.logpdf(data.θ.o[sampled_z[t, r]], data.y[t]);
                    end
                end
            end # end for (t ∈ {1, 2, ..., T} do)

            # Check if this is the best run and save it if so.
            if sample_log_lik > sweep_max_log_lik
                sweep_max_log_lik = sample_log_lik;
                sweep_max_sample_i = r;
                l = sample_l;
            end
        end

        # sample u_t ~ Uniform(0, l)
        for t = 1:T
            u[t] = rand(Distributions.Uniform(0, l[t]));
        end

        print(sweep_max_log_lik);
        print(" (");
        print(round(valid_samples * 100));
        print("% valid samples)");

        if sweep_max_log_lik > maximum(sampled_log_lik) || isnan(maximum(sampled_log_lik))
            println(" (New highest likelihood)");
            sampled_z_best = sampled_z[:, sweep_max_sample_i];
            sampled_d_best = sampled_d[:, sweep_max_sample_i];
            α_best = α;
        else
            println("");
        end

        z_stored[sweep, :] = sampled_z[:, sweep_max_sample_i]';

        sampled_log_lik[sweep] = sweep_max_log_lik;

        # sample parameters A, λ, θ
    end # end for (sweep ∈ {1, 2, 3, ...} do)

    # Prepare data for plotting.
#    max_duration = zeros(S, 1);
#
#    for t = 1:T
#        for s = S_VALID
#            max_duration[s] = length(α_best[t, s]);
#        end
#    end
#
#    max_duration = ceil(max_duration / 10) * 10;
#    for s = 1:S
#        max_duration[s] = min(max_duration[s], 50);
#    end
#
#    # Plot the "best" alpha-matrix and the first n matricies.
#    for α_plot_i = 0:2
#        α_maxtrix = zeros(T, convert(Int64, sum(max_duration)));
#
#        if α_plot_i == 0
#            α_this = α_best;
#        else
#            α_this = α_stored[α_plot_i]
#        end
#
#        for t = 1:T
#            s_start = 0;
#            for s = S_VALID
#                for d = 1:max_duration[s]
#                    if d <= length(α_this[t, s])
#                        α_maxtrix[t, s_start + d] = α_this[t, s][d];
#                    else
#                        break;
#                    end
#                end
#                s_start += max_duration[s]
#            end
#        end
#
#        α_plot_data = [
#          [
#            "x" => data.x,
#            #"y" => ["Morning", "Afternoon", "Evening"],
#            "z" => α_maxtrix,
#            "type" => "heatmap"
#          ]
#        ]
#
#        if α_plot_i == 0
#            response = try
#                Plotly.plot([α_plot_data], ["filename" => "edhmm-beam-sampler-alpha-matrix", "fileopt" => "overwrite"]);
#            catch
#                {"url" => "Error sending plot."};
#            end
#            print("Plotting α matrix for best sweep : ");
#        else
#            response = try
#                Plotly.plot([α_plot_data], ["filename" => string("edhmm/edhmm-beam-sampler-alpha-", α_plot_i), "fileopt" => "overwrite"]);
#            catch
#                {"url" => "Error sending plot."};
#            end
#            print("Plotting α matrix for sweep ");
#            print(α_plot_i);
#            print(" : ");
#        end
#
#        plot_url = response["url"];
#        println(plot_url);
#    end

    u_plot_data = [
      [
        "x" => data.x,
        #"y" => ["Morning", "Afternoon", "Evening"],
        "z" => u_stored',
        "type" => "heatmap"
      ]
    ]

    response = try
        Plotly.plot([u_plot_data], ["filename" => "edhmm-beam-sampler-u", "fileopt" => "overwrite"]);
    catch
        {"url" => "Error sending plot."};
    end
    plot_url = response["url"];
    print("Plotting u for all sweeps : ");
    println(plot_url);

    z_plot_data = [
      [
        "x" => data.x,
        #"y" => ["Morning", "Afternoon", "Evening"],
        "z" => z_stored',
        "type" => "heatmap"
      ]
    ]

    response = try
        Plotly.plot([z_plot_data], ["filename" => "edhmm-beam-sampler-z", "fileopt" => "overwrite"]);
    catch
        {"url" => "Error sending plot."};
    end
    plot_url = response["url"];
    print("Plotting z for all sweeps : ");
    println(plot_url);

    l_plot_data = [
      [
        "x" => data.x,
        #"y" => ["Morning", "Afternoon", "Evening"],
        "z" => l_stored',
        "type" => "heatmap"
      ]
    ]

    response = try
        Plotly.plot([l_plot_data], ["filename" => "edhmm-beam-sampler-l", "fileopt" => "overwrite"]);
    catch
        {"url" => "Error sending plot."};
    end
    plot_url = response["url"];
    print("Plotting l for all sweeps : ");
    println(plot_url);

    sampled_state = [
        "x" => data.x,
        "y" => sampled_z_best,
        "mode" => "lines",
        "line" => ["shape" => "hvh"],
        "type" => "scatter",
        "name" => "Inferred state"
    ];

    true_state = [
        "x" => data.x,
        "y" => data.z + S + 1,
        "mode" => "lines",
        "line" => ["shape" => "hvh"],
        "type" => "scatter",
        "name" => "True state"
    ];

    result_plot_data = [true_state, sampled_state];

    duration = [
        "x" => data.x,
        "y" => ((sampled_d_best / (maximum(sampled_d_best))) * S) + S + 1,
        "mode" => "lines",
        "line" => ["shape" => "hvh"],
        "type" => "scatter",
        "name" => "Inferred duration remaining"
    ];

    sample_plot_data = [sampled_state, duration];

    if multivariate
        for i = 1:size(data.y,1)
            observed = [
                "x" => data.x,
                "y" => data.y[i, :],
                "mode" => "lines",
                "line" => ["shape" => "hvh"],
                "type" => "scatter",
                "name" => string("Observed data ", i)
            ];

            sample_plot_data = [sample_plot_data, observed];
        end
    else
        observed = [
            "x" => data.x,
            "y" => data.y,
            "mode" => "lines",
            "line" => ["shape" => "hvh"],
            "type" => "scatter",
            "name" => "Observed data"
        ];

        sample_plot_data = [sample_plot_data, observed];
    end

    response = try
        Plotly.plot([sample_plot_data], ["filename" => "edhmm-beam-sampler-sampled", "fileopt" => "overwrite"]);
    catch
        {"url" => "Error sending plot."};
    end
    print("Plotting inferred states for best backward sample : ");
    plot_url = response["url"];
    println(plot_url);

    response = try
        Plotly.plot([result_plot_data], ["filename" => "edhmm-beam-sampler-result", "fileopt" => "overwrite"]);
    catch
        {"url" => "Error sending plot."};
    end
    print("Plotting result comparison for best backward sample : ");
    plot_url = response["url"];
    println(plot_url);

    sampler_log_liklihood = [
        "x" => [1:SWEEPS],
        "y" => sampled_log_lik',
        "mode" => "lines",
        "line" => ["shape" => "hvh"],
        "type" => "scatter",
        "showlegend" => false
    ];

    layout = [
      "xaxis" => [
        "title" => "Sweep"
      ],
      "yaxis" => [
        "title" => "Log Likelihood"
      ]
    ];

    response = try
        Plotly.plot([sampler_log_liklihood], ["layout" => layout, "filename" => "edhmm-beam-sampler-log-likelihood", "fileopt" => "overwrite"]);
    catch
        {"url" => "Error sending plot."};
    end
    plot_url = response["url"];
    print("Plotting maximum log-likelihood per sweep : ");
    println(plot_url);

    println(string("EDHMM percent correct: ", correctrate(int64(data.z), int64(sampled_z_best))));

    return sampled_z_best, sampled_log_lik, u_stored, z_stored, l_stored;
end

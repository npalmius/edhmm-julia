#################################################

cd("C:\\Research\\git\\edhmm-julia")

#################################################

using EDHMM
using Distributions

θ_uv=edhmm_init()
θ_mv=edhmm_init_mv()

include("edhmm_generative.jl")

data_uv=edhmm_generative(θ_uv)
data_mv=edhmm_generative(θ_mv)

include("edhmm_beamsampler.jl")

result_uv = edhmm_beam_sample(data_uv)[1];
result_mv = edhmm_beam_sample(data_mv)[1];

confusmat(length(data_uv.θ.o), int64(data_uv.z), int64(result_uv))
confusmat(length(data_mv.θ.o), int64(data_mv.z), int64(result_mv))
